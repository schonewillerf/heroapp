package ng.hero.backend;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

// The @Entity annotation creates a table in the DB
// Default table name is class name, so the Hero table is created
@Data
@NoArgsConstructor
@Entity
public class Hero
{
    // @Id is to infor Spring which field is the Id value
    // @Generated value will automatically be inserted
    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private int id;

    private String name;

    // Empty constructor used by JPA in order to fetch data
    // This can be

    // Another constructor used in CommandLineRunner in main
    public Hero( String name )
    {
        this.name = name;
    }

    // Standard getters and setters
    // The can be omitted now we have the @Data annotation
}
