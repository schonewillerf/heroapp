package ng.hero.backend;

import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

// By extending from JpaRepository we get access to default methods such as findById() findAll() save()
// Hero objects can be fetched and saved from DB Hero table using this repository
public interface HeroRepository extends JpaRepository<Hero, Integer>
{
    // Some custom methods are available by providing their names
    List<Hero> findAllByNameContaining( String term );

    // This interface is not implemented in any class yet it still works
    // Spring Boot has implemented the interface for us
    // This means we do not need to write SQL queries :)
}
