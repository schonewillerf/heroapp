package ng.hero.backend;

import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

// This is the RestController which wil make endpoints available over http
@AllArgsConstructor
@RestController
@RequestMapping( "/api" )
public class HeroController
{
    // Is instantiated with @AllArgsConstructor
    private final HeroRepository heroRepository;

    // navigate to http://localhost:8080/api/heroes to see JSON response
    @GetMapping( "/heroes" )
    public List<Hero> getHeroes()
    {
        return heroRepository.findAll();
    }

    // navigate to http://localhost:8080/api/heroes/1 for example to see JSON response
    @GetMapping( "/heroes/{id}" )
    public Hero getHero( @PathVariable int id ) throws Exception
    {
        return heroRepository.findById( id )
                .orElseThrow( () -> new Exception( "Hero with id not found" ) );
    }

    // navigate to http://localhost:8080/api/heroes/?name=milan to see JSON response
    @GetMapping( "/heroes/" )
    public List<Hero> searchHero( @RequestParam String name )
    {
        return heroRepository.findAllByNameContaining( name );
    }

    // Following methods are slightly more difficult to check in a browser
    @PostMapping( "/heroes" )
    public Hero addHero( @RequestBody Hero toBeAddedHero )
    {
        return heroRepository.save( toBeAddedHero );
    }

    @PutMapping( "/heroes" )
    public Hero updateHero( @RequestBody Hero toBeUpdatedHero )
    {
        return heroRepository.save( toBeUpdatedHero );
    }

    @DeleteMapping( "/heroes/{id}" )
    public Map<String, Boolean> deleteHero( @PathVariable int id )
    {
        heroRepository.deleteById( id );

        Map<String, Boolean> response = new HashMap<>();
        response.put( "deleted", Boolean.TRUE );

        return response;
    }
}
