package ng.hero.backend;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BackendApplication implements CommandLineRunner
{
	@Autowired
	private HeroRepository heroRepository;

	// Main method starting Spring Application
	// Has lots of default features so we get by with little code
    public static void main( String[] args )
    {
        SpringApplication.run( BackendApplication.class, args );
    }

    // My command line runner is executed at startup
	@Override
	public void run( String... args ) throws Exception
	{
		// This removes all Hero object from DB
		// Just to be sure there aren't any
		heroRepository.deleteAll();

		// Here new Hero's are added to DB
		// It is an in memory H2 database
		// Database is specified in pom.xml, and can easily be replaced with MySQL or MongoDB for example
		heroRepository.save( new Hero( "Milan" ) );
		heroRepository.save( new Hero( "Jan" ) );
		heroRepository.save( new Hero( "Ruben" ) );
		heroRepository.save( new Hero( "Rico" ) );
		heroRepository.save( new Hero( "Thom" ) );
		heroRepository.save( new Hero( "Raymen" ) );
	}
}
