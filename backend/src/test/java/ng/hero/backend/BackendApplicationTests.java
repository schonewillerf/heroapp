package ng.hero.backend;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
class BackendApplicationTests {

	@Test
	void contextLoads() {
	}

	@Test
	public void testHeroName()
	{
		Hero hero = new Hero( "Raymen" );

		assertEquals( "Raymen", hero.getName(), "expected Hero name is Raymen" );
	}

}
