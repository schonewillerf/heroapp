# HeroApp

Example of Angular frontend with Spring Boot backend

## Usage

In order to improve developer experience some extra dependencies are added 
to the Spring backend project. You might have to install a plugin for Lombok
processing in your IDE.

Furthermore, in order to fully profit from hot reloading both in the Spring 
backend, and the Angular ToH frontend we have to serve Angular with the 
development server.

Inside the frontend folder do:
```
npm install
ng serve
```
Inside the backend folder do:
```
./mvnw spring-boot:run
```

Visit http://localhost:8080/ for Database Console and API Documentation & Testing
Visit http://localhost:4200/ for Angular frontend




